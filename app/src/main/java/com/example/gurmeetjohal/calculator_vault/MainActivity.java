package com.example.gurmeetjohal.calculator_vault;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static DecimalFormat df2 = new DecimalFormat(".########");
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private TextView screen;
    private String display = "";
    private String currentOperator = "";
    private String hasresult = "";
    private String code = "4155";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        screen = (TextView) findViewById(R.id.btnedit);
        System.out.println("test");
        if (display.equals("")) {
            Log.d(display, "display is null");
        } else {
            screen.setText(display);
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void updatescreen() {
        screen.setText(display);
    }

    public void onclickNumber(View v) {
        if (hasresult != "") {
            clear();
            updatescreen();
        }
        Button number = (Button) v;
        display += number.getText();
        updatescreen();
    }

    public void onclickOperator(View v) {
        if (display == "") return;
        if (hasresult != "") {
            display = hasresult;
            hasresult = "";
        }
        Button operator = (Button) v;
        display += operator.getText();
        currentOperator = operator.getText().toString();
        updatescreen();
    }

    public double operate(String opr1, String opr2, String operator) {
        double result;
        switch (operator) {
            case "+":
                result = Double.parseDouble(opr1) + Double.parseDouble(opr2);
                return result;
            case "-":
                result = Double.parseDouble(opr1) - Double.parseDouble(opr2);
                return result;
            case "*":
                result = Double.parseDouble(opr1) * Double.parseDouble(opr2);
                return result;
            case "/":
                try {
                    result = Double.parseDouble(opr1) / Double.parseDouble(opr2);

                    return result;
                } catch (Exception e) {

                    Log.d("calc", e.getMessage());
                }

            default:
                return Double.parseDouble(screen.getText().toString());
        }

    }

    public void onclickEqual(View v) {
        try {
            if (Integer.parseInt(display) == 4155) {
                Intent intent = new Intent(this, PhotoGallary.class);

                startActivity(intent);          // Start New Activity From this Method
                Log.d("equal method", "worked");
            } else {
                String[] operation = display.split(Pattern.quote(currentOperator));
                if (operation.length < 2) return;
                Double ans = operate(operation[0], operation[1], currentOperator);
                if (ans % 2 == 0) {
                    hasresult = String.valueOf(ans.intValue());
                    screen.setText(hasresult);
                } else {
                    hasresult = df2.format(ans);
                    screen.setText(hasresult);
                }

            }

        } catch (Exception e) {
            Log.d("equal method", e.getMessage());
        }

    }

    public void clear() {
        display = "";
        currentOperator = "";
        hasresult = "";
    }

    public void onclickClear(View v) {
        clear();
        updatescreen();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.gurmeetjohal.calculator_vault/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.gurmeetjohal.calculator_vault/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}


























